%{

open Quad
open Common
open Comp
open Karel

type pair = {a: int; b: int};;
type triplet = {a:int; b:int; c:int};;

%}

%token BEGIN_PROG
%token BEGIN_EXEC
%token END_EXEC
%token END_PROG

%token MOVE
%token TURN_LEFT
%token TURN_OFF

%token SEMI
%token BEGIN
%token END

%token PUT_BEEPER PICK_BEEPER
%token NEXT_TO_A_BEEPER  NOT_NEXT_TO_A_BEEPER
%token FRONT_IS_CLEAR  FRONT_IS_BLOCKED
%token LEFT_IS_CLEAR LEFT_IS_BLOCKED
%token RIGHT_IS_CLEAR RIGHT_IS_BLOCKED
%token FACING_NORTH NOT_FACING_NORTH
%token FACING_EAST NOT_FACING_EAST
%token FACING_SOUTH NOT_FACING_SOUTH
%token FACING_WEST NOT_FACING_WEST
%token ANY_BEEPERS_IN_BEEPER_BAG NO_BEEPERS_IN_BEEPER_BAG
%token IF WHILE DO THEN ELSE
%token ITERATE TIMES
%token DEFINE_NEW_INSTRUCTION AS

%token <int> INT
%token <string> ID

%type <unit> prog
%start prog

%%

begin_main:{
	nextquad ()
}
begin_prog:{
	let a = nextquad () in 
	let _ = gen(GOTO (0)) in 
	a
}

prog:	
BEGIN_PROG begin_prog sousProg BEGIN_EXEC begin_main stmts_opt END_EXEC END_PROG{
	backpatch $2 $5;
	()
};

sousProg:
/*Empty*/{
	()
}
| sousProg define_new {
	()
};
begin_sous_prog:{
	nextquad ()
}

define_new:
DEFINE_NEW_INSTRUCTION ID AS begin_sous_prog stmts{
	if is_defined $2 then 
		raise(SyntaxError "this was already used")
	else 
		gen(RETURN);
		(define $2 $4)
};

stmts_opt:	
/* empty */ { 
	() 
}
|stmts	{ 
	() 
};

stmts:		
stmt { 
	() 
}
| stmts SEMI stmt	{ 
	() 
}
|stmts SEMI { 
	() 
};


simple_stmt: 
TURN_LEFT	{ 
	gen (INVOKE (turn_left, 0, 0)) 
}
|TURN_OFF { 
	gen STOP  
}
|MOVE { 
	gen (INVOKE (move, 0, 0)) 
}
|PICK_BEEPER {
	gen (INVOKE (pick_beeper, 0, 0))
}  
|PUT_BEEPER {
	gen (INVOKE (put_beeper, 0, 0))
}
|ID {
	let ident = get_define $1 in
	gen (CALL (ident))
};

test: 
FRONT_IS_CLEAR {
	let d = new_temp() in gen (INVOKE(is_clear, front, d));
	d
}
|FRONT_IS_BLOCKED {
	let d = new_temp() in gen (INVOKE(is_blocked, front, d));
	d
}
|LEFT_IS_CLEAR {
	let d = new_temp() in gen (INVOKE(is_clear, left, d));
	d
}
|LEFT_IS_BLOCKED {
	let d = new_temp() in gen (INVOKE(is_blocked, left, d));
	d
}
|RIGHT_IS_CLEAR {
	let d = new_temp() in gen (INVOKE(is_clear, right, d));
	d
}
|RIGHT_IS_BLOCKED {
	let d = new_temp() in gen (INVOKE(is_blocked, right, d));
	d
}
|NEXT_TO_A_BEEPER {
	let d = new_temp() in gen (INVOKE(next_beeper, d, 0));
	d
}
|NOT_NEXT_TO_A_BEEPER {
	let d = new_temp() in gen (INVOKE(no_next_beeper, d, 0));
	d
}
|FACING_NORTH {
	let d = new_temp() in gen (INVOKE(facing, north, d));
	d
}
|NOT_FACING_NORTH{
	let d = new_temp() in gen (INVOKE(not_facing, north, d));
	d
}
|FACING_EAST {
	let d = new_temp() in gen (INVOKE(facing, east, d));
	d
}
|NOT_FACING_EAST {
	let d = new_temp() in gen (INVOKE(not_facing, east, d));
	d
}
|FACING_SOUTH {
	let d = new_temp() in gen (INVOKE(facing, south, d));
	d
}
|NOT_FACING_SOUTH {
	let d = new_temp() in gen (INVOKE(not_facing, south, d));
	d
}
|FACING_WEST {
	let d = new_temp() in gen (INVOKE(facing, west, d));
	d
}
|NOT_FACING_WEST {
	let d = new_temp() in gen (INVOKE(not_facing, west, d));
	d
}
|ANY_BEEPERS_IN_BEEPER_BAG {
	let d = new_temp() in gen (INVOKE(any_beeper, d, 0));
	d
}
|NO_BEEPERS_IN_BEEPER_BAG {
	let d = new_temp() in gen (INVOKE(no_beeper, d, 0));
	d
};
/*IF*/
if_test:test {
	let v' = new_temp() in
	let _ = gen (SETI(v',0)) in
	let a = nextquad () in
	let _ = gen (GOTO_EQ (0,$1,v')) in
	a
};
/*WHILE*/ 
begin_while:{
	nextquad()
};
pre_while_bloc: begin_while test {
	let v' = new_temp() in
	let _ = gen (SETI(v',0)) in
	let a = nextquad () in
	let _ = gen (GOTO_EQ (0,$2,v')) in
	{a=$1; b=a}
};
end_while: {
	let a = nextquad () in 
	let _ = gen(GOTO (0)) in 
	a
}
open_while_block: open_stmt end_while{
	$2
};
close_while_block: closed_stmt end_while{
	$2
};
/*ITERATE*/
init_iterate: INT{
	let v' = new_temp() in 
	let _ = gen(SETI (v',0)) in
	let v''= new_temp() in
	let _ = gen(SETI(v'',$1) ) in
	let a = nextquad () in 
	{a=v'; b=v''; c=a}
};


pre_iterate_bloc: init_iterate {
	let a = nextquad () in
	let _ = gen (GOTO_EQ (0,$1.a,$1.b)) in
	{a=$1.a;b=a}
};
/*else*/
end_then:{
	let a = nextquad() in
	let _ = gen(GOTO (0)) in 
	a
}
begin_else:{
	nextquad()
}

/*-----*/
open_stmt:
IF if_test THEN stmt {
	backpatch $2 (nextquad ());
	()
}
|IF if_test THEN closed_stmt end_then ELSE begin_else open_stmt {
	backpatch $2 ($7);
	backpatch $5 (nextquad ());
	()
}
| WHILE pre_while_bloc DO open_while_block {
	let _ = gen(GOTO ($2.b)) in
	()
}
| ITERATE pre_iterate_bloc TIMES open_stmt {
	let a = new_temp () in
	let _ = gen(SETI (a,1)) in
	gen(ADD ($2.a, $2.a, a));
	let _ = gen(GOTO ($2.b)) in
	backpatch $2.b (nextquad ());
	()
};


closed_stmt:
non_if_stmt {
	()
}
|IF if_test THEN closed_stmt end_then ELSE begin_else closed_stmt {
	backpatch $2 ($7);
	backpatch $5 (nextquad ());
	()
}
| WHILE  pre_while_bloc DO close_while_block {
	backpatch $2.b (nextquad ());
	backpatch $4 ($2.a);
	()
} 
|ITERATE pre_iterate_bloc TIMES closed_stmt  {
	let a = new_temp () in
	let _ = gen(SETI (a,1)) in
	gen(ADD ($2.a, $2.a, a));
	let _ = gen(GOTO ($2.b)) in
	backpatch $2.b (nextquad ());
	()
};




non_if_stmt:
BEGIN stmts_opt END {
	()
}
| simple_stmt {
	()
};

stmt:
open_stmt{
	()
}
|closed_stmt
{
	()
}
;