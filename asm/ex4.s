@
@ Question 5.2
@
@ Ecrivez un programme qui fait un parcours vertical depuis
@ sa position courante jusqu’à ce qu’il trouve un beeper.
@ Il partira vers le nord puis, quand il trouvera un mur,
@ il tournera à gauche et avancera avant de repartir vers
@ le sud et ainsi de suite.
@
_start:
	seti r0, #1
	seti r1, #0 @ beeper trouver
	seti r2, #0 @ il y a un mur
	seti r3, #0 @ sens tourne 
	seti r4, #0 @ tourne gauche 
loop:
	@ si on n'est pas bloquer en face 
	invoke 6, 1, 2
	goto_eq else, r2, r0
	invoke 1, 0, 0
	goto endif
else:
	@ sinon demi tour
	goto_ne tourne_droite, r3, r4
	invoke 2, 0, 0
	invoke 1, 0, 0 
	invoke 2, 0, 0
	seti r4, #1
	goto  endturn
tourne_droite:
	invoke 2, 0, 0
	invoke 2, 0, 0
	invoke 2, 0, 0
	invoke 1, 0, 0 
	invoke 2, 0, 0
	invoke 2, 0, 0
	invoke 2, 0, 0
	seti r4, #0
endturn:
endif:
	@ si le beeper est trouver alors on ne fait pas le goto
	invoke 11, 1, 0
	goto_ne loop, r1, r0

	stop
