@
@ Question 3.2.2
@
@ En utilisant une boucle, réalisez un programme permettant
@ au robot de se déplacer 5 fois vers le sud.
@
_start:
	invoke 2, 0, 0
	invoke 2, 0, 0
	seti r0, #0
	seti r1, #5
	seti r3, #1
loop:
	invoke 1, 0, 0
	sub r1, r1, r3
	goto_ne loop, r0 ,r1 
	stop
